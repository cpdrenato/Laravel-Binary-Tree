<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BinaryTree extends Model {
    public function insert($key, $value = null) {
        $node = new Node([
            'key' => $key,
            'value' => $value,
        ]);
        if ($this->root === null) {
            $this->root = $node;
            $node->save();
            return;
        }
        $current = $this->root;
        while (true) {
            if ($key < $current->key) {
                if ($current->leftChild === null) {
                    $current->leftChild()->save($node);
                    $node->parent()->associate($current)->save();
                    break;
                }
                $current = $current->leftChild;
            } elseif ($key > $current->key) {
                if ($current->rightChild === null) {
                    $current->rightChild()->save($node);
                    $node->parent()->associate($current)->save();
                    break;
                }
                $current = $current->rightChild;
            } else {
                $current->value = $value;
                $current->save();
                break;
            }
        }
    }

    public function search($key) {
        $current = $this->root;
        while ($current !== null) {
            if ($key < $current->key) {
                $current = $current->leftChild;
            } elseif ($key > $current->key) {
                $current = $current->rightChild;
            } else {
                return $current;
            }
        }
        return null;
    }
}

