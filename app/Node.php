<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Node extends Model {
    protected $fillable = ['key', 'value', 'parent_id', 'left_child_id', 'right_child_id'];

    public function parent() {
        return $this->belongsTo(Node::class, 'parent_id');
    }

    public function leftChild() {
        return $this->hasOne(Node::class, 'id', 'left_child_id');
    }

    public function rightChild() {
        return $this->hasOne(Node::class, 'id', 'right_child_id');
    }

    public function children()
    {
        return $this->hasMany(Node::class, 'parent_id', 'id');
    }
}
