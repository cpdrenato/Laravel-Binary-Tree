<?php

// Criação de um array com a estrutura da árvore genealógica
$familia = array(
    'avo_paterno' => array(
        'pai' => array(
            'filho' => array(),
            'filha' => array(),
        ),
        'tia' => array(),
    ),
    'avo_materno' => array(
        'mae' => array(
            'filho' => array(),
            'filha' => array(),
        ),
        'tio' => array(),
    ),
);

// Função para exibir a árvore genealógica recursivamente
function exibir_arvore($arvore, $nivel = 0)
{
    foreach ($arvore as $key => $value) {
        echo str_repeat('--', $nivel) . $key . "<br>";
        if (!empty($value)) {
            exibir_arvore($value, $nivel + 1);
        }
    }
}

// Exibição da árvore genealógica
exibir_arvore($familia);

// Arvore binaria abaixo com PHP puro

class Node {
    public $key;
    public $value;
    public $left = null;
    public $right = null;

    public function __construct($key, $value = null) {
        $this->key = $key;
        $this->value = $value;
    }
}

class BinaryTree {
    public $root = null;

    public function insert($key, $value = null) {
        $node = new Node($key, $value);
        if ($this->root === null) {
            $this->root = $node;
            return;
        }
        $current = $this->root;
        while (true) {
            if ($key < $current->key) {
                if ($current->left === null) {
                    $current->left = $node;
                    break;
                }
                $current = $current->left;
            } elseif ($key > $current->key) {
                if ($current->right === null) {
                    $current->right = $node;
                    break;
                }
                $current = $current->right;
            } else {
                $current->value = $value;
                break;
            }
        }
    }

    public function search($key) {
        $current = $this->root;
        while ($current !== null) {
            if ($key < $current->key) {
                $current = $current->left;
            } elseif ($key > $current->key) {
                $current = $current->right;
            } else {
                return $current->value;
            }
        }
        return null;
    }
}

// Exemplo de uso:
$tree = new BinaryTree();
$tree->insert(10, 'Valor 10');
$tree->insert(5, 'Valor 5');
$tree->insert(15, 'Valor 15');
$tree->insert(2, 'Valor 2');
$tree->insert(7, 'Valor 7');
$tree->insert(12, 'Valor 12');
$tree->insert(20, 'Valor 20');

echo $tree->search(7); // Output: Valor 7
