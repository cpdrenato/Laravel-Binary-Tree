<?php

namespace App\Http\Controllers;

use App\Node;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BinaryTreeController extends Controller
{
    public function index(Request $request)
    {
        $nodes = Node::query();

        if ($request->has('parent_id')) {
            $nodes->where('parent_id', $request->parent_id);
        }

        $nodes = $nodes->get();

        $tree = [];

        foreach ($nodes as $node) {
            $tree[$node->id] = [
                'id' => $node->id,
                'key' => $node->key,
                'value' => $node->value,
                'parent_id' => $node->parent_id,
                'children' => [],
            ];
        }

        foreach ($tree as &$node) {
            if ($node['parent_id']) {
                $tree[$node['parent_id']]['children'][] = &$node;
            }
        }

        $tree = array_values(array_filter($tree, function ($node) {
            return !$node['parent_id'];
        }));

        return response()->json([
            'success' => true,
            'message' => 'Binary tree retrieved successfully',
            'data' => $tree,
        ]);
    }

    public function indexView()
    {
        $root = Node::where('parent_id', null)->first();
        return view('binary-tree.__index', compact('root'));
    }

    public function create()
    {
        DB::table('nodes')->truncate();
        Node::create([
            'key' => 50,
            'value' => 'Root',
        ]);
        Node::create([
            'key' => 30,
            'value' => 'Left child of root',
            'parent_id' => 1,
        ]);
        Node::create([
            'key' => 70,
            'value' => 'Right child of root',
            'parent_id' => 1,
        ]);
        Node::create([
            'key' => 20,
            'value' => 'Left child of left child',
            'parent_id' => 2,
        ]);
        Node::create([
            'key' => 40,
            'value' => 'Right child of left child',
            'parent_id' => 2,
        ]);
        Node::create([
            'key' => 60,
            'value' => 'Left child of right child',
            'parent_id' => 3,
        ]);
        Node::create([
            'key' => 80,
            'value' => 'Right child of right child',
            'parent_id' => 3,
        ]);
        return 'Binary tree created successfully';

    }

    public function insert(Request $request)
    {
        $this->validate($request, [
            'key' => 'required|integer|unique:nodes',
            'value' => 'nullable|string',
            'parent_id' => 'nullable|integer|exists:nodes,id',
        ]);

        $node = Node::create([
            'key' => $request->key,
            'value' => $request->value,
            'parent_id' => $request->parent_id,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Node inserted successfully',
            'data' => $node,
        ]);
    }

    public function search(Request $request)
    {
        $this->validate($request, [
            'key' => 'required|integer',
        ]);

        $node = Node::where('key', $request->key)->first();

        if (!$node) {
            return response()->json([
                'success' => false,
                'message' => 'Node not found',
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Node found',
            'data' => $node,
        ]);
    }

}
