<?php

// namespace Database\Seeders;

use App\Node;
use Illuminate\Database\Seeder;

class BinaryTreeSeeder extends Seeder
{
    // php artisan db:seed --class=BinaryTreeSeeder

    public function run()
    {
        Node::query()->delete();

        $root = Node::create([
            'key' => 50,
            'value' => 'A',
        ]);

        $left = Node::create([
            'key' => 30,
            'value' => 'B',
            'parent_id' => $root->id,
        ]);

        $right = Node::create([
            'key' => 70,
            'value' => 'C',
            'parent_id' => $root->id,
        ]);

        $leftLeft = Node::create([
            'key' => 20,
            'value' => 'D',
            'parent_id' => $left->id,
        ]);

        $leftRight = Node::create([
            'key' => 40,
            'value' => 'E',
            'parent_id' => $left->id,
        ]);

        $rightLeft = Node::create([
            'key' => 60,
            'value' => 'F',
            'parent_id' => $right->id,
        ]);

        $rightRight = Node::create([
            'key' => 80,
            'value' => 'G',
            'parent_id' => $right->id,
        ]);
    }
}

