@extends('layouts.app')

@section('content')
    <h1>Binary Tree</h1>
    <div class="binary-tree">
        <ul>
            <li>{{ $root->value }}</li>
            @foreach($root->children as $child)
                @include('binary-tree.tree', ['node' => $child])
            @endforeach
        </ul>
    </div>
@endsection
