<li>{{ $node->value }}</li>
@if(count($node->children) > 0)
    <ul>
        @foreach($node->children as $child)
            @include('binary-tree.tree', ['node' => $child])
        @endforeach
    </ul>
@endif
