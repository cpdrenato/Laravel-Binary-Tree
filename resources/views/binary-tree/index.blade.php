<!DOCTYPE html>
<html>
<head>
    <title>Binary Tree</title>
</head>
<body>
    <ul>
        <li>{{ $root->value }}</li>
        @foreach($root->children as $child)
            @include('binary-tree.tree', ['node' => $child])
        @endforeach
    </ul>
</body>
</html>
